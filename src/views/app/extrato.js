import React, { Component, Fragment } from "react";
import { injectIntl } from 'react-intl';
import {
  Row,
  Card,
  CardBody,
  CardTitle,
  NavLink,
  CardSubtitle,
  CardImg,
  CardText,
  Button
} from "reactstrap";

import { Colxx, Separator } from "../../components/common/CustomBootstrap";
import Breadcrumb from "../../containers/navs/Breadcrumb";

import IconCardsCarousel from "../../containers/dashboards/IconCardsCarousel";
import RecentOrders from "../../containers/dashboards/RecentOrders";
import Logs from "../../containers/dashboards/Logs";
import Tickets from "../../containers/dashboards/Tickets";
import Calendar from "../../containers/dashboards/Calendar";
import BestSellers from "../../containers/dashboards/BestSellers";
import ProfileStatuses from "../../containers/dashboards/ProfileStatuses";
import GradientCardContainer from "../../containers/dashboards/GradientCardContainer";
import Cakes from "../../containers/dashboards/Cakes";
import GradientWithRadialProgressCard from "../../components/cards/GradientWithRadialProgressCard";
import SortableStaticticsRow from "../../containers/dashboards/SortableStaticticsRow";
import AdvancedSearch from "../../containers/dashboards/AdvancedSearch";
import SmallLineCharts from "../../containers/dashboards/SmallLineCharts";
import SalesChartCard from "../../containers/dashboards/SalesChartCard";
import ProductCategoriesPolarArea from "../../containers/dashboards/ProductCategoriesPolarArea";
import WebsiteVisitsChartCard from "../../containers/dashboards/WebsiteVisitsChartCard";
import ConversionRatesChartCard from "../../containers/dashboards/ConversionRatesChartCard";
import TopRatedItems from "../../containers/dashboards/TopRatedItems";

class DefaultDashboard extends Component {
  render() {
    const { messages } = this.props.intl;
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12">
            <Breadcrumb heading="menu.default" match={this.props.match} />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        <Row>
          <Colxx lg="12" xl="6" className="mb-4"> 
            <SalesChartCard />
          </Colxx>
          <Colxx lg="12" xl="6" className="mb-2">
            <Row>
              <Colxx lg="4" xl="12" className="mb-4">

                <Card>
                           <div>
                    <CardBody>
                      <div>
                        <NavLink to="/app/ui/cards">
                          <CardSubtitle>Valor Investido no Período</CardSubtitle>
                        </NavLink>
                        <CardText className="text-muted text-large mb-2">R$ 80.000,00</CardText>
                        
                      </div>
                    </CardBody>
                  </div>
                </Card>
              </Colxx>
              <Colxx lg="4" xl="12" className="mb-2">
                <Card>
                  <CardBody >
                      <div >
                        <NavLink to="/app/ui/cards">
                          <CardSubtitle>Saldo</CardSubtitle>
                        </NavLink>
                        <CardText className="text-muted text-large mb-2">R$ 45.000,00</CardText>
                      </div>
                    </CardBody>
                </Card>
              </Colxx>

            </Row>
          </Colxx>
        </Row>
        <Separator className="mb-5" />

        <Row>
          <Colxx xl="12" lg="12" className="mb-4">
            <BestSellers />
          </Colxx>
        </Row>




      </Fragment>
    );
  }
}
export default injectIntl(DefaultDashboard);