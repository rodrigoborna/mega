import React, { useState } from 'react';
import { Table, Input, InputNumber, Popconfirm, Form } from 'antd';
import 'antd/dist/antd.css';
import sourceData from "../../../../data/products.json";
import dataFilter from "../../../../data/dataFilter";


const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Favor informar o ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};
const EditableTable = () => {
  const arr = sourceData;
  const [form] = Form.useForm();
  const [data, setData] = useState(arr);
  const [editingKey, setEditingKey] = useState('');

  const isEditing = record => record.cat_id === editingKey;

  const edit = record => {
    form.setFieldsValue({
      cat_id: '',
      name: '',
      bid_value: '',
      min:'',
      max:'',
      sort_value:'',
      ...record,
    });
    setEditingKey(record.cat_id);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async cat_id => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex(item => cat_id === item.cat_id);
     
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
        setData(newData);
        setEditingKey('');
        
        
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey('');
        
      }
    } catch (errInfo) {
      console.log('Validação Falhou:', errInfo);
    }
  };

  const columns = [
    {
      title: 'ID Categoria',
      dataIndex: 'cat_id',
      width: '15%',
      align: 'center',
      sorter: (a, b) => a.cat_id- b.cat_id,
      sortDirections: ['descend', 'ascend'],
      editable: false,
    },
    {
      title: 'Categoria',
      filtered:'true',
      dataIndex: 'name',
      width: '30%',
      align: 'center',
      editable: false,
      filters: dataFilter,
      filterMultiple: true,
      onFilter: (value, record) => record.title.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: 'CPC Atual',
      dataIndex: 'bid_value',
      width: '15%',
      align: 'center',
      sorter: (a, b) => a.bid_value - b.bid_value,
      sortDirections: ['descend', 'ascend'],
      editable: false,
    },
    {
        title: 'CPC Min.',
        dataIndex: 'min',
        width: '15%',
        align: 'center',
        sorter: (a, b) => a.min - b.min,
        sortDirections: ['descend', 'ascend'],
        editable: false,
      },
      {
        title: 'CPC Max.',
        dataIndex: 'max',
        width: '15%',
        align: 'center',
        sorter: (a, b) => a.max - b.max,
        sortDirections: ['descend', 'ascend'],
        editable: false,
      },
      {
        title: 'CPC Novo',
        dataIndex: 'sort_value',
        width: '15%',
        align: 'center',
        editable: true,
      },
    {
      title: 'Operação',
      align: 'center',
      dataIndex: 'operation',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <a
              href="javascript:;"
              onClick={() => save(record.cat_id)}
              style={{
                marginRight: 8,
              }}
            >
              Salvar
            </a>
            <Popconfirm title="Quer Cancelar?" onConfirm={cancel}>
              <a>Cancelar</a>
            </Popconfirm>
          </span>
        ) : (
          <a disabled={editingKey !== ''} onClick={() => edit(record)}>
            Personalizar
          </a>
        );
      },
    },
  ];
  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        inputType: col.dataIndex === 'cat_id' ? 'number' : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });
 
      return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={{
          onChange: cancel,
        }}
      />
    </Form>
  );
};
 export default (EditableTable);
