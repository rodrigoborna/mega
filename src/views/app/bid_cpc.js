import React, { Component, Fragment } from "react";
import { Row, Card, CardBody, CardTitle, Table } from "reactstrap";
import IntlMessages from "../../helpers/IntlMessages";
import { Colxx, Separator } from "../../components/common/CustomBootstrap";
import Breadcrumb from "../../containers/navs/Breadcrumb";
import TabCardExamples from "../../containers/ui/TabCardExamples";
import ClickToSelectTable from "../../containers/ui/teste";

import {
  ReactTableAdvancedCard
} from "../../containers/ui/ReactTableCards";

export default class TablesUi extends Component {
  render() {
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12">
          <Breadcrumb heading="menu.bidagem" match={this.props.match} />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        
        <TabCardExamples />
        
       
      </Fragment>
    );
  }
}
