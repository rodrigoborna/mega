import React, { Component } from "react";

import {
  Row,
  Card,
  CardBody,
  CardTitle,
  CardHeader,
  Nav,
  NavItem,
  TabContent,
  InputGroup,
  InputGroupAddon,
  Button,
  CustomInput,
  TabPane
} from "reactstrap";
import { NavLink } from "react-router-dom";

import classnames from "classnames";
import IntlMessages from "../../helpers/IntlMessages";
import { Colxx } from "../../components/common/CustomBootstrap";
import {
  ReactTableAdvancedCard
} from "../../containers/ui/ReactTableCards";
import EditableTable from  "../../views/app/report/bid/bid";

class TabCardExamples extends Component {
  constructor(props) {
    super(props);

    this.toggleFirstTab = this.toggleFirstTab.bind(this);
    this.toggleSecondTab = this.toggleSecondTab.bind(this);
    this.state = {
      activeFirstTab: "1",
      activeSecondTab: "1"
    };
  }

  toggleFirstTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeFirstTab: tab
      });
    }
  }
  toggleSecondTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeSecondTab: tab
      });
    }
  }
  render() {
    return (
      <Row>
        <Colxx xxs="12">

          <Row>
            <Colxx xxs="12">
              <Card className="mb-4">
                <CardHeader>
                  <Nav tabs className="card-header-tabs ">
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activeFirstTab === "1",
                          "nav-link": true
                        })}
                        onClick={() => {
                          this.toggleFirstTab("1");
                        }}
                        to="#"
                      >
                        Bid Categoria
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activeFirstTab === "2",
                          "nav-link": true
                        })}
                        onClick={() => {
                          this.toggleFirstTab("2");
                        }}
                        to="#"
                      >
                        Bid Oferta
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activeFirstTab === "3",
                          "nav-link": true
                        })}
                        onClick={() => {
                          this.toggleFirstTab("3");
                        }}
                        to="#"
                      >
                        Bidagem em Lote
                      </NavLink>
                    </NavItem>
                  </Nav>
                </CardHeader>

                <TabContent activeTab={this.state.activeFirstTab}>
                  <TabPane tabId="1">
                    <Row>
                      <Colxx sm="12">
                        <CardBody >
                          <CardTitle className="mb-4">
                            Bidagem por Categorias
                          </CardTitle>
                          
                          <EditableTable />
                          
                        </CardBody>
                      </Colxx>
                    </Row>
                  </TabPane>
                  <TabPane tabId="2">
                    <Row>
                      <Colxx sm="12">
                        <CardBody>
                          <CardTitle className="mb-4">
                            Bidagem por Ofertas
                          </CardTitle>
                          <ReactTableAdvancedCard />
                        </CardBody>
                      </Colxx>
                    </Row>
                  </TabPane>
                  <TabPane tabId="3">
                    <Row>
                      <Colxx sm="12">
                        <CardBody>
                          <CardTitle className="mb-4">
                            Envie seu arquivo para Bidagem em Lote
                          </CardTitle>
                          <bidEditableTable />
                          <InputGroup className="mb-3">
                            <CustomInput
                              type="file"
                              id="exampleCustomFileBrowser2"
                              name="customFile"
                            />
                            <InputGroupAddon addonType="append">Enviar</InputGroupAddon>
                          </InputGroup>
                        </CardBody>
                      </Colxx>
                    </Row>
                  </TabPane>
                </TabContent>
              </Card>
            </Colxx>


          </Row>
        </Colxx>
      </Row>
    );
  }
}

export default TabCardExamples;
