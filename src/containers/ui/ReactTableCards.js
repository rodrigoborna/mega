import React from "react";
import { Card, CardBody, CardTitle, Button } from "reactstrap";
import PerfectScrollbar from "react-perfect-scrollbar";
import ReactTable from "react-table";
import classnames from "classnames";
import IntlMessages from "../../helpers/IntlMessages";
import DataTablePagination from "../../components/DatatablePagination";


import data from "../../data/products";

const CustomTbodyComponent = props => (
  <div {...props} className={classnames("rt-tbody", props.className || [])}>
    <PerfectScrollbar option={{ suppressScrollX: true }}>
      {props.children}
    </PerfectScrollbar>
  </div>
);



const dataTableColumns = [
  {
    Header: "ID Categoria",
    width:100,
    accessor: "id",
    Cell: props => <p className="list-item-heading">{props.value}</p>
  },
  {
    Header: "Categoria",
    width:200,
    accessor: "title",
    Cell: props => <p className="text-muted">{props.value}</p>
  },
  {
    Header: "CPC Atual",
    width:100,
    accessor: "sales",
    Cell: props => <p className="text-muted">{props.value}</p>
  },
  {
    Header: "CPC Min.",
    width:100,
    accessor: "min",
    Cell: props => <p className="text-muted">{props.value}</p>
  },
  {
    Header: "CPC Max.",
    width:100,
    
    accessor: "max",
    Cell: props => <p style={{textAlign:"center"}} className="text-muted">{props.value}</p>
  },
  {
    Header: "CPC Novo",
    width:100,
    accessor: "sales",
    Cell: props => <input style={{textAlign:"center",width:"70px"}}  placeholder={props.value}/>
  },
  {
    width:90,
    filterable:false,
    Cell: props => <p><Button class="icon-button large ml-1 btn btn-outline-primary" outline onClick={() => alert("atualizar")} size="sm" color="primary"><i class="iconsminds-save"></i></Button></p>
  }
];


export const ReactTableAdvancedCard = props => {
  return (
    
        <ReactTable
          data={data}
          columns={dataTableColumns}
          defaultPageSize={5}
          filterable={false}
          showPageJump={true}
          
          PaginationComponent={DataTablePagination}
          showPageSizeOptions={true}
          sortDirections={true}
        />
     
  );
};
