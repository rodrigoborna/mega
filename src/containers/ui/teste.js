import React from "react";
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';
import data from "../../data/products";

const columns = [{
  dataField: 'id',
  text: 'Product ID'
}, {
  dataField: 'sales',
  text: 'Product Name'
}, {
  dataField: 'price',
  text: 'Product Price'
}];

      export const testeTableClick = props => {
               return (
                <BootstrapTable
                keyField="id"
                data={ data }
                columns={ columns }
                cellEdit={ cellEditFactory({ mode: 'click' }) }
              />
          );
        }
     